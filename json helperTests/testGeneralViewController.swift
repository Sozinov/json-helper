//
//  testGeneralViewController.swift
//  json helperTests
//
//  Created by Nikolay S on 22.09.2020.
//

import XCTest

class testGeneralViewController: XCTestCase {
    let storyboard = NSStoryboard(name: "Main", bundle: Bundle(for: GeneralViewController.self))

    override func setUpWithError() throws {
        super .setUp()
    }

    override func tearDownWithError() throws {
        super .tearDown()
    }
    
    func testStartGeneral() {
        let generalViewController = storyboard.instantiateController(withIdentifier: String(describing: GeneralViewController.self)) as! GeneralViewController
        generalViewController.loadView()
        
        //MARK: chek property
        XCTAssertNotNil(generalViewController.progressIndicator, "progressIndicator is nil")
        XCTAssertTrue(generalViewController.progressIndicator.isDescendant(of: generalViewController.view))
        XCTAssertNotNil(generalViewController.showBtn, "showBtn is nil")
        XCTAssertTrue(generalViewController.showBtn.isDescendant(of: generalViewController.view))
        XCTAssertNotNil(generalViewController.jsonTextView, "jsonTextView is nil")
        XCTAssertTrue(generalViewController.jsonTextView.isDescendant(of: generalViewController.view))
        XCTAssertNotNil(generalViewController.oulineView, "oulineView is nil")
        XCTAssertTrue(generalViewController.oulineView.isDescendant(of: generalViewController.view))
        
        //MARK: chek outlineView  DataSource, Delegate
       
    }

}

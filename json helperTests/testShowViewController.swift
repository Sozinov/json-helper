//
//  testShowViewController.swift
//  json helper
//
//  Created by Nikolay S on 21.09.2020.
//

import XCTest

class testShowViewController: XCTestCase {

    let newStoryboard = NSStoryboard(name: "Main", bundle: Bundle(for: ShowViewController.self))
    
    override func setUpWithError() throws {
        super .setUp()
    }

    override func tearDownWithError() throws {
        super .tearDown()
    }
    // MARK: Test ShowViewController
    func testShowViewController() {
        let showViewController = newStoryboard.instantiateController(withIdentifier: String(describing: ShowViewController.self)) as! ShowViewController
        showViewController.loadView()
        
        XCTAssertNotNil(showViewController.structTextView, "structTextView is nil")
        XCTAssertTrue(showViewController.structTextView.isDescendant(of: showViewController.view))
        XCTAssertNotNil(showViewController.classTextView, "classTextView is nil")
        XCTAssertTrue(showViewController.classTextView.isDescendant(of: showViewController.view))
        
        XCTAssertNotNil(showViewController.viewDidLoad(), "viewDidLoad is nil")
    }

}

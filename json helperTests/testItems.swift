//
//  testItems.swift
//  json helperTests
//
//  Created by Nikolay S on 28.09.2020.
//

import XCTest

class testItems: XCTestCase {
    let sutItem = Item.self
    
    override func setUpWithError() throws {
        super .setUp()
        
    }
    
    override func tearDownWithError() throws {
        super .tearDown()
    }
    func testitemsFromString() {
        
        let testJsonString = "{\"string\":\"Hello World\",\"boolean\":true,\"temp\":null,\"dictionary\":{\"a\":\"b\",\"c\":\"d\"},\"array\":[1,2,3],\"number\":123,\"color\":\"gold\"}"
        XCTAssertNotNil(sutItem.itemsFromString(testJsonString), "testJsonString is nil")
    }
    
}

//
//  json_helperTests.swift
//  json helperTests
//
//  Created by Nikolay S on 21.09.2020.
//

import XCTest
@testable import json_helper

class json_helperTests: XCTestCase {

    let storyboard = NSStoryboard(name: "Main", bundle: nil)
    let sutAppDelegate = AppDelegate()
    
    override func setUpWithError() throws {
        super .setUp()
    }

    override func tearDownWithError() throws {
        super .tearDown()
    }
// MARK: Test History menu
    func testHistoryMenuCountMoreZerro() {
        sutAppDelegate.chekMenuItemArray()
        let menuItemArray = sutAppDelegate.menuItemArray
        XCTAssert(menuItemArray.count > 0, "history menu is absent")
    }
    func testHistoryMenuCountZerro() {
        let menuItemArray = sutAppDelegate.menuItemArray
        XCTAssert(menuItemArray.count == 0, "history menu is not absent")
    }
    
    func testPushMenuAction() {
        sutAppDelegate.chekMenuItemArray()
        let menuItemArray = sutAppDelegate.menuItemArray
        menuItemArray.forEach { (menuItem) in
            XCTAssertNotNil(sutAppDelegate.menuItemAction(menuItem), "menuItemAction is nil")
        }
    }
    // MARK: Test About
    func testInfoAbout() {
        let aboutController = storyboard.instantiateController(withIdentifier: String(describing: AboutViewController.self)) as! AboutViewController
        aboutController.loadView()
        
        XCTAssertNotNil(aboutController.infoTextField, "infoTextField is nil")
        XCTAssertTrue(aboutController.infoTextField.isDescendant(of: aboutController.view))
        XCTAssertNotNil(aboutController.closeBtnAction(self), "closeBtnAction is nil")
        
        XCTAssertNotNil(aboutController.titleTextView, "titleTextView is nil")
        XCTAssertTrue(aboutController.titleTextView.isDescendant(of: aboutController.view))
        XCTAssertNotNil(aboutController.titleInfoBtnAction(self), "titleInfoBtnAction is nil")
        
        XCTAssertNotNil(aboutController.licenseBtn, "licenseBtn is nil")
        XCTAssertTrue(aboutController.licenseBtn.isDescendant(of: aboutController.view))
        XCTAssertNotNil(aboutController.licenseBtnAction(self), "licenseBtnAction is nil")
        
        XCTAssertNotNil(aboutController.infoTextInScrollView, "infoTextInScrollView is nil")
        XCTAssertTrue(aboutController.infoTextInScrollView.isDescendant(of: aboutController.view))

    }
}

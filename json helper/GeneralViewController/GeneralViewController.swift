//
//  GeneralViewController.swift
//  json helper
//
//  Created by Nikolay S on 22.09.2020.
//

import Cocoa

class GeneralViewController: NSViewController, NSOutlineViewDelegate, NSOutlineViewDataSource {
    
    @IBOutlet weak var showBtn: NSButton!
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
    @IBOutlet var jsonTextView: NSTextView!
    @IBOutlet weak var oulineView: NSOutlineView!
    @IBOutlet weak var successErrorImageView: NSImageView!
    
    var items: [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showBtn.title = "Show".localized
        progressIndicator.isHidden = true
        successErrorImageView.isHidden = true
        
        self.oulineView.delegate = self
        self.oulineView.dataSource = self
        self.jsonTextView.delegate = self
        
        self.jsonTextView.lnv_setUpLineNumberView()
        
        showLoader()
        // Test data
        DispatchQueue.main.asyncAfter(wallDeadline: .now() + 1.5) {
            self.jsonTextView.string = self.createTestJSON().rawString() ?? ""
            self.hideLoader()
            self.oulineView.reloadData()
        }
    }
    
    private func showLoader() {
        self.progressIndicator.isHidden = false
        self.progressIndicator.startAnimation(self)
    }
    private func hideLoader() {
        self.progressIndicator.isHidden = true
        self.progressIndicator.stopAnimation(self)
    }
    func updateStatusIndicator(status:Bool) {
        if self.jsonTextView.string.count > 0 {
            successErrorImageView.isHidden = false
            successErrorImageView.image = status ? NSImage(named: .success) : NSImage(named: .failure)
            animationStatusHide()
        } else {
            successErrorImageView.isHidden = true
            successErrorImageView.alphaValue = 0
        }
    }
    func animationStatusHide() {
        successErrorImageView.animations.removeAll()
        successErrorImageView.alphaValue = 0
        NSAnimationContext.runAnimationGroup({ (context) in
            context.duration = 2
            self.successErrorImageView.animator().alphaValue = 1
        }) {
             self.successErrorImageView.alphaValue = 1
        }
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "showIdentifier" {
            if let showViewController = segue.destinationController as? ShowViewController {
                showViewController.items = items
            }
        }
    }
 // Test data
    private func createTestJSON() -> JSON {
          return JSON(
              ["key_string": "string value",
               "key_int": 3,
               "key_bool": true,
               "key_float": 3.4,
               "key_dont_show": JSON.null,
               "key_array": ["string new", "random_stuff", 356],
               "key_dic": ["sub_key": "value", "sub_key_second": false],
               "key_any": [
                   [
                       "sub_key_Float": 4.5,
                       "double_Double": Double(5.3),
                       "sub_key_String": "value",
                       "internal": "renamed_value",
                       "sub_key_Dic": ["two_level_down": "value"],
                   ],
               ],
               "key_ArrEmpty": []])
      }
}





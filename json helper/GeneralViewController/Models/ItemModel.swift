//
//  MainViewController.swift
//  json helper
//
//  Created by Nikolay S on 21.09.2020.
//

import Cocoa

enum TypeItem: String, Codable {
    case string = "String"
    case number = "Number"
    case int = "Int"
    case float = "Float"
    case double = "Double"
    case array = "Array"
    case dictionary = "Dictionary"
    case bool = "Bool"
    case null = ""
}

struct Item: Codable {
    let key: String
    let value: String
    let type: TypeItem
    var childrens: [Item] = []
    var isArray:Bool?
    
    /// Create a helper function to recursivly create items
    ///
    /// - Parameters:
    ///   - parents: number of parent items
    ///   - childrens: number of children for each parent item
    /// - Returns: array of Item
    
    private static func childrensItems(_ key:String, _ array: Array<Any>) -> [Item] {
        var items: [Item] = []
        array.enumerated().forEach { (index, value) in
            switch value {
            case let stringValue as String:
                items.append(Item(key: "\(index)", value: "\(stringValue)", type: .string, childrens: [], isArray: true))
            case let boolValue as Bool:
                items.append(Item(key: "\(index)", value: "\(boolValue)", type: .bool, childrens: [], isArray: true))
            case let intValue as Int:
                items.append(Item(key: "\(index)", value: "\(intValue)", type: .int, childrens: [], isArray: true))
            case let floatValue as Float:
                items.append(Item(key: "\(index)", value: "\(floatValue)", type: .float, childrens: [], isArray: true))
            case let doubleValue as Double:
                items.append(Item(key: "\(index)", value: "\(doubleValue)", type: .double, childrens: [], isArray: true))
            case let numberValue as NSNumber:
                items.append(Item(key: "\(index)", value: "\(numberValue)", type: .number, childrens: [], isArray: true))
            case let arrayValue as Array<Any> :
                let result = childrensItems(key, arrayValue)
                items.append(Item(key: "\(arrayValue)", value: "\(result.count)", type: .array, childrens: result))
            case let dictionaryValue as [String:Any]:
                let result = parentItems(dictionaryValue)
                items.append(Item(key: "\(index)", value: "\(dictionaryValue.count)", type: .dictionary, childrens: result, isArray: true))
            case _ as NSNull:
                items.append(Item(key: "\(key)", value: "null", type: .null, childrens: []))
            default:
                break
            }
        }
        return items
    }
    
    private static func parentItems(_ dictionary:[String:Any]) -> [Item] {
       var items: [Item] = []
        dictionary.forEach { (value) in
            switch value.value {
            case let stringValue as String:
                items.append(Item(key: "\(value.key)", value: "\(stringValue)", type: .string, childrens: []))
            case let boolValue as Bool:
                items.append(Item(key: "\(value.key)", value: "\(boolValue)", type: .bool, childrens: []))
            case let intValue as Int:
                items.append(Item(key: "\(value.key)", value: "\(intValue)", type: .int, childrens: []))
            case let floatValue as Float:
                items.append(Item(key: "\(value.key)", value: "\(floatValue)", type: .float, childrens: []))
            case let doubleValue as Double:
                items.append(Item(key: "\(value.key)", value: "\(doubleValue)", type: .double, childrens: []))
            case let numberValue as NSNumber:
                items.append(Item(key: "\(value.key)", value: "\(numberValue)", type: .number, childrens: []))
            case let arrayValue as Array<Any> :
                let result = childrensItems("\(value.key)", arrayValue)
                items.append(Item(key: "\(value.key)", value: "\(result.count)", type: .array, childrens: result))
            case let dictionaryValue as [String:Any]:
                let result = parentItems(dictionaryValue)
                items.append(Item(key: "\(value.key)", value: "\(dictionaryValue.count)", type: .dictionary, childrens: result))
            case _ as NSNull:
                items.append(Item(key: "\(value.key)", value: "null", type: .null, childrens: []))
            default:
                break
            }
        }
        return items
    }
    
    static func itemsFromString(_ string:String) -> ([Item], Bool) {
        var items: [Item] = []
        var status = false
        guard string.count > 0 else {
            return (items, false)
        }
        let data = string.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data) as? [String:Any]
            {
//                print(jsonArray) // use the json here
                status = true
                if jsonArray.count > 0 {
                    items = parentItems(jsonArray)
                }
            } else {
                #if DEBUG
                print("bad json")
                #endif
                status = false
            }
        } catch let errorJson as NSError {
            #if DEBUG
            print(errorJson)
            #endif
            status = false
        }
        return (items.sorted(by: {$0.key > $1.key}), status)
    }
}

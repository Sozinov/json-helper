//
//  CellsModel.swift
//  json helper
//
//  Created by Nikolay S on 22.09.2020.
//

import Cocoa

class HeaderCell: NSTableCellView {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
}

class MainCell: NSTableCellView {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
}

class AddCell: NSTableCellView {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
}

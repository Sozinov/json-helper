//
//  GeneralTextViewDelegate.swift
//  json helper
//
//  Created by Nikolay S on 28.09.2020.
//

import Cocoa
extension GeneralViewController: NSTextViewDelegate {
    
    func textDidChange(_ notification: Notification) {
        guard let textView = notification.object as? NSTextView else { return }
        #if DEBUG
           print("textDidChange")
        #endif
     self.oulineView.reloadData()
        
    }
    func textView(_ textView: NSTextView, willChangeSelectionFromCharacterRanges oldSelectedCharRanges: [NSValue], toCharacterRanges newSelectedCharRanges: [NSValue]) -> [NSValue] {
        
        let formatString = jsonTextView.string.replacingOccurrences(of: "”", with: "\"").replacingOccurrences(of: "“", with: "\"")
        textView.string = formatString
        let resultParse = Item.itemsFromString(formatString)
        self.items = resultParse.0
        self.updateStatusIndicator(status: resultParse.1)
        textView.lineNumberView.needsDisplay = true
        return newSelectedCharRanges
    }
}

//
//  GenerarTable.swift
//  json helper
//
//  Created by Nikolay S on 28.09.2020.
//

import Cocoa
extension GeneralViewController {
    
    // сами ячейки
    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if item == nil {
            return items[index]
        }
        
        if let item = item as? Item, item.childrens.count > index {
            return item.childrens[index]
        }
        return ""
    }
    // разворачиваем или нет
    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        guard let item = item as? Item else {
            return false
        }
        return !item.childrens.isEmpty
    }
    // кол-во ячеек
    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if item == nil {
            return items.count
        }
        if let item = item as? Item {
            return item.childrens.count
        }
        return 0
    }
    // ячейки для кадой tableColumn
    func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        
        guard let item = item as? Item else {
            return nil
        }
        
        let headerCell = outlineView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "HeaderCell"), owner: self) as! HeaderCell
        switch tableColumn?.identifier.rawValue {
        case "Key":
            if let tf = headerCell.textField {
                tf.stringValue = item.key
                switch item.type {
                case .string:
                    tf.textColor = .string
                case .bool:
                    tf.textColor = .bool
                case .int, .float, .double:
                    tf.textColor = .number
                case .null:
                    tf.textColor =  .lightGray
                default:
                    tf.textColor = .someObject
                }
                if item.isArray ?? false {
                    tf.textColor =  NSColor.lightGray.withAlphaComponent(0.5)
                }
                tf.sizeToFit()
            }
        case "Value":
            if let tf = headerCell.textField {
                tf.stringValue = item.value
                switch item.type {
                case .string:
                    tf.textColor = .string
                case .bool:
                    tf.textColor = .bool
                case .int, .float, .double:
                    tf.textColor = .number
                case .null:
                    tf.textColor = NSColor.lightGray.withAlphaComponent(0.5)
                default:
                    tf.textColor = .someObject
                }
                tf.sizeToFit()
            }
        case "Type":
            if let tf = headerCell.textField {
                tf.stringValue = item.type.rawValue
                switch item.type {
                case .string:
                    tf.textColor = .lowGreen
                case .bool:
                    tf.textColor = .lowGreen
                case .number:
                    tf.textColor = .lowGreen
                default:
                    tf.textColor = .someObject
                }
                tf.sizeToFit()
            }
        default:
            break
        }
        return headerCell
    }
    
    func outlineViewSelectionDidChange(_ notification: Notification) {
        guard let outlineView = notification.object as? NSOutlineView else {
            return
        }
        outlineView.deselectAll(nil)
    }
    
    // TODO: Запомнить состояние чтобы при перезагрузке оставалось так. как сделал юзер
    func outlineViewItemWillExpand(_ notification: Notification) {
        #if DEBUG
        print("open")
        #endif
    }
    func outlineViewItemDidCollapse(_ notification: Notification) {
        #if DEBUG
        print("close")
        #endif
    }
    
    func test() {
        self.oulineView.expandItem(nil)
    }
}

//
//  AboutViewController.swift
//  json helper
//
//  Created by Nikolay S on 21.09.2020.
//

import Cocoa

class AboutViewController: NSViewController {

    @IBOutlet weak var titleTextView: NSTextField!
    @IBOutlet weak var titleInfoBtn: NSButton!
    @IBOutlet weak var infoTextField: NSTextField!
    @IBOutlet var infoTextInScrollView: NSTextView!
    @IBOutlet weak var licenseBtn: NSButton!
    @IBOutlet weak var closeBtn: NSButton!
    
    private var isClicBtnFlag = true
    
    private var appName: String {
        return Bundle.main.appName ?? ""
    }
    
    private var appVersion: String {
        let version = Bundle.main.buildVersionNumber ?? ""
        let shortVersion = Bundle.main.releaseVersionNumber ?? ""
        return "Version \(shortVersion) (Build \(version))"
    }
    private var appCopyright: String {
        return Bundle.main.copyright ?? ""
    }
    override func viewWillAppear() {
        super.viewWillAppear()
        self.view.window?.minSize = NSSize(width: 510, height: 300)
        self.view.window?.maxSize = NSSize(width: 510, height: 300)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "AboutJH".localized
        titleInfoBtn.addCursorRect(titleInfoBtn.bounds, cursor: .pointingHand)
        titleTextView.stringValue = appName
        infoTextField.stringValue = appVersion
        licenseBtn.stringValue = "License Agreement".localized
        closeBtn.title = "Close".localized
        infoTextInScrollView.string = "InfoTextAbout".localized
    }
    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(nil)
    }
    @IBAction func licenseBtnAction(_ sender: Any) {
        isClicBtnFlag = !isClicBtnFlag
        infoTextInScrollView.string = isClicBtnFlag ? "InfoTextAbout".localized : appCopyright
        licenseBtn.title = isClicBtnFlag ? "License Agreement".localized : "AboutJH".localized
    }
    
    @IBAction func titleInfoBtnAction(_ sender: Any) {
        let url = URL(string: "http://nik.sozinov.com/eng/")!
        if NSWorkspace.shared.open(url) {
            #if DEBUG
            print("default browser was successfully opened")
            #endif

        }
    }
    
}

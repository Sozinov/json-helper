//
//  NameGenerator.swift
//  json helper
//
//  Created by Nikolay S on 12.10.2020.
//

import Foundation
struct NameGenerator {

    static func fixVariableName(_ variableName: String) -> String {
        var tmpVariableName = replaceKeywords(variableName)
        tmpVariableName.replaceOccurrencesOfStringsWithString(["-", "_"], " ")
        tmpVariableName.trim()
        
        var finalVariableName = ""
        for (index, var element) in tmpVariableName.components(separatedBy: " ").enumerated() {
            index == 0 ? element.lowercaseFirst() : element.uppercaseFirst()
            finalVariableName.append(element)
        }
        return finalVariableName
    }
    
    static func replaceKeywords(_ currentName: String) -> String {
        let keywordsWithReplacements = [
            "description": "descriptionValue",
            "class": "classProperty",
            "struct": "structProperty",
            "enum": "enumProperty",
            "internal": "internalProperty",
            "default": "defaultValue",
        ]
        if let value = keywordsWithReplacements[currentName] {
            return value
        }
        return currentName
    }
}

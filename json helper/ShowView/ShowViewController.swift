//
//  ShowViewController.swift
//  json helper
//
//  Created by Nikolay S on 21.09.2020.
//

import Cocoa

class ShowViewController: NSViewController {
    
    var items:[Item] = []

    @IBOutlet var structTextView: NSTextView!
    @IBOutlet var classTextView: NSTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        structTextView.string = FileGenerator.generateFileContentWith(items, constructType: .structType)
        classTextView.string = FileGenerator.generateFileContentWith(items, constructType: .classType)
    }
}

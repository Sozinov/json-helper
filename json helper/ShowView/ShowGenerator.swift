//
//  ShowGenerator.swift
//  json helper
//
//  Created by Nikolay S on 09.10.2020.
//

import Foundation

enum ConstructType: String {
    case classType = "class"
    case structType = "struct"
}

struct FileGenerator {
    
    static func loadFileWith(_ filename: String) throws -> String {
        let bundle = Bundle.main
        guard let path = bundle.path(forResource: filename, ofType: "txt") else {
            return ""
        }
        let content = try String(contentsOfFile: path)
        return content
    }
    
    static func generateFileContentWith(_ items:[Item], constructType: ConstructType) -> String {
        var content = try! loadFileWith("BaseTemplate")
        let singleTab = "  ", doubleTab = "    ", equalTab = " = ", quotesTab = "\"", _ = "try"
        content = content.replacingOccurrences(of: "{OBJECT_NAME}", with: "Model") // modelFile.fileName
        content = content.replacingOccurrences(of: "{DATE}", with: todayDateString())
        content = content.replacingOccurrences(of: "{OBJECT_KIND}", with: constructType.rawValue) // modelFile.type.rawValue
        
        content = content.replacingOccurrences(of: "__NAME__", with: "Nikolay")
        content = content.replacingOccurrences(of: "__MyCompanyName__", with: "CompanyName")
        

        let stringConstants = items.map {doubleTab + "case" + singleTab + NameGenerator.fixVariableName($0.key) + equalTab + quotesTab + $0.key + quotesTab}.joined(separator: "\n")
        /*
        "  var keyTwo: Int?\n  var keyFour: Float?\n  var keySix: KeySix?\n  var keyThree: Bool?\n  var keyOne: String?\n  var keySeven: [KeySeven]?\n  var keyEight: Any?\n  var keyFive: [String]?"
 */

        let declarations = items.map {singleTab + "var" + singleTab + NameGenerator.fixVariableName($0.key) + ":" + singleTab + ($0.type.rawValue.count > 0 ? $0.type.rawValue : "Any") + "?"}.joined(separator: "\n")
        
        /*
         "    keyThree = try container.decodeIfPresent(Bool.self, forKey: .keyThree)\n    keyFour = try container.decodeIfPresent(Float.self, forKey: .keyFour)\n    keySeven = try container.decodeIfPresent([KeySeven].self, forKey: .keySeven)\n    keyTwo = try container.decodeIfPresent(Int.self, forKey: .keyTwo)\n    keyOne = try container.decodeIfPresent(String.self, forKey: .keyOne)\n    keyFive = try container.decodeIfPresent([String].self, forKey: .keyFive)\n    keySix = try container.decodeIfPresent(KeySix.self, forKey: .keySix)\n    keyEight = try container.decodeIfPresent([].self, forKey: .keyEight)"
         */
        let initialisers = items.map { doubleTab + NameGenerator.fixVariableName($0.key) + " = try container.decodeIfPresent(" + ($0.type.rawValue.count > 0 ? $0.type.rawValue : "Any") + ".self, forKey: ." + NameGenerator.fixVariableName($0.key) + ")"}.joined(separator: "\n")
        
        
        content = content.replacingOccurrences(of: "{STRING_CONSTANT}", with: stringConstants)
        content = content.replacingOccurrences(of: "{DECLARATION}", with: declarations)
        content = content.replacingOccurrences(of: "{INITIALIZER}", with: initialisers)
        
//        if modelFile.type == .classType {
//            content = content.replacingOccurrences(of: "{REQUIRED}", with: "required ")
//            if modelFile.configuration?.shouldGenerateInitMethod == true {
//                let assignment = modelFile.component.initialiserFunctionComponent.map { doubleTab + $0.assignmentString }.joined(separator: "\n")
//                let functionParameters = modelFile.component.initialiserFunctionComponent.map { $0.functionParameter }.joined(separator: ", ")
//                let initialiserFunctionStatement = "\n\(singleTab)init (\(functionParameters)) {"
//                content = content.replacingOccurrences(of: "{INITIALIZER_FUNCTION_DECLRATION}", with: initialiserFunctionStatement)
//                content = content.replacingOccurrences(of: "{INITIALISER_FUNCTION_ASSIGNMENT}", with: assignment)
//                content = content.replacingOccurrences(of: "{INITIALISER_FUNCTION_END}", with: "\(singleTab)}\n")
//            }
//        } else {
            content = content.replacingOccurrences(of: "{REQUIRED}", with: "")
            content = content.replacingOccurrences(of: "{INITIALIZER_FUNCTION_DECLRATION}", with: "")
            content = content.replacingOccurrences(of: "{INITIALISER_FUNCTION_ASSIGNMENT}", with: "")
            content = content.replacingOccurrences(of: "{INITIALISER_FUNCTION_END}", with: "")
//        }
        return content
    }
    
    fileprivate static func todayDateString() -> String {
         let formatter = DateFormatter()
         formatter.dateStyle = .short
         return formatter.string(from: Date())
     }
}

//
//  AppDelegate.swift
//  json helper
//
//  Created by Nikolay S on 21.09.2020.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @IBOutlet weak var newWindows: NSMenuItem!
    //MARK: add History menu
   // @IBOutlet weak var historyNSMenu: NSMenu!
    //var menuItemArray:[NSMenuItem] = []
    var isLastWindowClosedFlag = false
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        self.newWindows.isEnabled = false
        newWindows.attributedTitle = attributed(isLastWindowClosedFlag)
        // Insert code here to initialize your application
//        self.chekMenuItemArray()
//        menuItemArray.forEach({ (menu) in
//            historyNSMenu.addItem(menu)
//        })
    }
    
//    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
//        print(isShowMainWindowFlag)
//        return isShowMainWindowFlag
//    }
    
    func attributed(_ isLastWindowClosedFlag:Bool) -> NSAttributedString {
        switch isLastWindowClosedFlag {
        case true:
            self.newWindows.isEnabled = true
            return NSAttributedString(string: "New App Windows", attributes: [NSAttributedString.Key.font: NSFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: NSColor.black])
        default:
            self.newWindows.isEnabled = false
            return NSAttributedString(string: "New App Windows", attributes: [NSAttributedString.Key.font: NSFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: NSColor.gray])
        }
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        isLastWindowClosedFlag = true
        newWindows.attributedTitle = attributed(isLastWindowClosedFlag)
        return false
    }

    @IBAction func newWindowsMenuAction(_ sender: Any) {
        if isLastWindowClosedFlag {
            let storyboard = NSStoryboard(name: "Main", bundle: nil)
            if let myWindowController = storyboard.instantiateController(withIdentifier: "MainWindow") as? NSWindowController {
                isLastWindowClosedFlag = false
                newWindows.attributedTitle = attributed(isLastWindowClosedFlag)
                myWindowController.showWindow(self)
            }
        }
    }
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        #if DEBUG
        print("")
        #endif
    }

    @objc func menuItemAction(_ sender: NSMenuItem) {
        #if DEBUG
        print("push menu \(sender.tag)")
        #endif
    }
//    //TODO: It is TEST data
//    func chekMenuItemArray() {
//        menuItemArray.removeAll()
//        let menuItem = NSMenuItem(title: "History test 1", action: #selector(menuItemAction), keyEquivalent: "")
//        menuItem.tag = 1
//        let menuItem2 = NSMenuItem(title: "History test 2", action: #selector(menuItemAction), keyEquivalent: "")
//        menuItem2.tag = 2
//        menuItemArray.append(menuItem)
//        menuItemArray.append(menuItem2)
//    }
//
    // add Core Data for History
    lazy var persistentContainer: NSPersistentContainer = {

            let container = NSPersistentContainer(name: "Model")
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            })
            return container
        }()

        // MARK: - Core Data Saving support
        
        func saveContext () {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }

}


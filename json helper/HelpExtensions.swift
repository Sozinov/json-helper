//
//  HelpExtensions.swift
//  json helper
//
//  Created by Nikolay S on 21.09.2020.
//

import Foundation
import AppKit
import Cocoa

extension String {
    
    mutating func replaceOccurrencesOfStringsWithString(_ strings: [String], _ replacementString: String) {
        for string in strings {
            self = replacingOccurrences(of: string, with: replacementString)
        }
    }
    /// Removes whitespace and newline at the ends.
    mutating func trim() {
        self = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    /// Fetches the first character of the string.
     var firstChar: String {
         return String(self.prefix(1))
     }
    /// Makes the first character of the string uppercase.
    mutating func uppercaseFirst() {
        self = firstChar.uppercased() + dropFirst()
    }

    /// Makes the first character of the string lowercase.
    mutating func lowercaseFirst() {
        self = firstChar.lowercased() + dropFirst()
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    var nsstring: NSString {
        return self as NSString
    }
    // for cell
    func heightForWidth(width: CGFloat, options: NSString.DrawingOptions = .usesLineFragmentOrigin, attributes: [NSAttributedString.Key: Any]? = nil, context: NSStringDrawingContext? = nil) -> CGFloat {
        return ceil(nsstring.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: options, attributes: attributes, context: context).height)
    }
    
    func widthForHeight(height: CGFloat, options: NSString.DrawingOptions = .usesLineFragmentOrigin, attributes: [NSAttributedString.Key: Any]? = nil, context: NSStringDrawingContext? = nil) -> CGFloat {
        return ceil(nsstring.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: height), options: options, attributes: attributes, context: context).width)
    }
}
extension NSImage.Name {
    static let success = NSImage.Name("success")
    static let failure = NSImage.Name("failure")
}
extension NSColor {
    static let string = NSColor.init(red: 255/255, green: 130/255, blue: 112/255, alpha: 1)
    static let bool = NSColor.init(red: 255/255, green: 122/255, blue: 179/255, alpha: 1)
    static let number = NSColor.init(red: 216/255, green: 202/255, blue: 124/255, alpha: 1)
    static let someObject = NSColor.init(red: 202/255, green: 173/255, blue: 234/255, alpha: 1)
    static let lowGreen = NSColor.init(red: 119/255, green: 193/255, blue: 178/255, alpha: 1)
}

 extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    var appName: String? {
        return infoDictionary?[kCFBundleNameKey as String] as? String
    }
    
    var copyright: String? {
        return infoDictionary?["NSHumanReadableCopyright"] as? String
    }
}

// Keyboard Shortcuts in Dialog without an Edit Menu
extension NSTextView {
override open func performKeyEquivalent(with event: NSEvent) -> Bool {
    let commandKey = NSEvent.ModifierFlags.command.rawValue
    let commandShiftKey = NSEvent.ModifierFlags.command.rawValue | NSEvent.ModifierFlags.shift.rawValue
    if event.type == NSEvent.EventType.keyDown {
        if (event.modifierFlags.rawValue & NSEvent.ModifierFlags.deviceIndependentFlagsMask.rawValue) == commandKey {
            switch event.charactersIgnoringModifiers! {
            case "x":
                if NSApp.sendAction(#selector(NSText.cut(_:)), to:nil, from:self) { return true }
            case "c":
                if NSApp.sendAction(#selector(NSText.copy(_:)), to:nil, from:self) { return true }
            case "v":
                if NSApp.sendAction(#selector(NSText.paste(_:)), to:nil, from:self) { return true }
            case "z":
                if NSApp.sendAction(Selector(("undo:")), to:nil, from:self) { return true }
            case "a":
                if NSApp.sendAction(#selector(NSResponder.selectAll(_:)), to:nil, from:self) { return true }
            default:
                break
            }
        } else if (event.modifierFlags.rawValue & NSEvent.ModifierFlags.deviceIndependentFlagsMask.rawValue) == commandShiftKey {
            if event.charactersIgnoringModifiers == "Z" {
                if NSApp.sendAction(Selector(("redo:")), to:nil, from:self) { return true }
            }
        }
    }
    return super.performKeyEquivalent(with: event)
}
}

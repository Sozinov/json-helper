# README #

Personal project, includes unit tests  
[Mac App Store](https://apps.apple.com/ru/app/jsonhelper/id1536132004)

### What is this? ###

* json parser for easy data display

### What is planned? ###

* Export class or structure
* Algorithm for parsing data from the Xcode debug console
* Creating Json from Data Table

### What is this for? ###

* Do something useful
* Discuss solutions and implementation in an interview
* Writing tests

### More? ###
[SwiftUI iOS App](https://bitbucket.org/Sozinov/swiftui/src/master/)

[New SwiftUI iOS App](https://bitbucket.org/Sozinov/testmegafon/src/master/README.md)